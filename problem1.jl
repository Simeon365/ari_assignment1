### A Pluto.jl notebook ###
# v0.18.4

using Markdown
using InteractiveUtils

# ╔═╡ 09505e60-adc4-11ec-2592-89305c03f79b
using Pkg

# ╔═╡ 201f0440-adcc-11ec-2d76-537d71d4e8c3
Pkg.activate("Project.toml")

# ╔═╡ dce0b3d0-adcc-11ec-3831-8ba580c24628
begin
	using DataFrames
    using CSV
    using Plots
    using GLM
    using Statistics
    using StatsPlots
    using MLBase
	using RDatasets
	using StatsBase
	using PlutoUI
	using Flux
end


# ╔═╡ 4a936f80-adcd-11ec-1f02-a3139bd6cc33
begin
	df = DataFrame(CSV.File("Real_estate.csv"))
	first(df,5)
end

# ╔═╡ 6b636db0-adef-11ec-1f9e-d1da52821ebb
begin
	using Lathe.preprocess: TrainTestSplit
	train, test = TrainTestSplit(df,.75)
end

# ╔═╡ d673fcb0-adee-11ec-1665-c3378544300c
begin
	colnames = Symbol[]
	for i in string.(names(df))
	    push!(colnames,Symbol(replace(replace(replace(strip(i)," " => "_"),"-" => "_"), "/" => "_")))
	end
	
	rename!(df, colnames);
end

# ╔═╡ 6bd36d90-adef-11ec-3757-351131787e2c
begin
X = real_estate_df[:, 3:7]
X_mat = Matrix(X)
Y = real_estate_df[:, 8]
Y_mat = Vector(Y)
training_size = 0.7
size_of_all_data = size(X_mat)[1]
training_index = trunc(Int, training_size * size_of_all_data)
X_mat_train = X_mat[1:training_index, :]
X_mat_test = X_mat[training_index+1:end, :]
Y_mat_train = Y_mat[1:training_index]
Y_mat_test = Y_mat[training_index+1:end]
end

# ╔═╡ 6b902300-adef-11ec-077f-29269cccae9e
function loss(features::Matrix{Float64}, outcome::Vector{Float64}, weights::Vector{Float64})::Float64
	m = size(features)[1]
	hypothesis = features * weights
	loss = hypothesis - outcome
	cost = (1/(2m)) * (loss' * loss)
	return cost
end

# ╔═╡ 6b787c50-adef-11ec-20aa-09b57ce48a95
function scaling_parameters(init_features::Matrix{Float64})::Tuple{Matrix{Float64}, Matrix{Float64}}
	feature_mean = mean(init_features, dims=1)
	f_dev = std(init_features, dims=1)
	return (feature_mean, f_dev)
end

# ╔═╡ 6b46e500-adef-11ec-0a0e-932d24773226
function scale_features(features::Matrix{Float64}, sc_params::Tuple{Matrix{Float64}, Matrix{Float64}})::Matrix{Float64}
	normalised_features = (features .- sc_params[1]) ./ sc_params[2]
end

# ╔═╡ 6b2d90a0-adef-11ec-1907-cf705875db7c
begin
scaling_params = scaling_parameters(X_mat_train)
scaling_params[1]
last(scaling_params)
scaled_training_features = scale_features(X_mat_train, scaling_params)
scaled_testing_features = scale_features(X_mat_test, scaling_params)
end

# ╔═╡ 6b2a3540-adef-11ec-2aa3-ff4ee1d89da0
function train_model(features::Matrix{Float64}, outcome::Vector{Float64}, alpha::Float64, n_iter::Int64)::Tuple{Vector{Float64}, Vector{Float64}}
	total_entry_count = length(outcome)
	aug_features = hcat(ones(total_entry_count, 1), features)
	feature_count = size(aug_features)[2]
	weights = zeros(feature_count)
	loss_vals = zeros(n_iter)

	for i in range(1, stop=n_iter)
		predict = aug_features * weights
		loss_vals[i] = get_loss(aug_features, outcome, weights)
		weights = weights - ((alpha/total_entry_count) * aug_features') * (pred - outcome)
	end
	
	return (weights, loss_vals)
end

# ╔═╡ 6afeb872-adef-11ec-2aec-153be0251d6c
function predict(features::Matrix{Float64}, weights::Vector{Float64})::Vector{Float64}
	total_entry_count = size(features)[1]
	aug_features = hcat(ones(total_entry_count, 1), features)
	preds = aug_features * weights
	return preds
end


# ╔═╡ d9e3d2ca-0ba1-42c9-b986-a5be115f622c


# ╔═╡ fb34b59a-481d-46cf-b545-f4a9cf376dbb


# ╔═╡ 05b1a808-d1b4-4a12-a15b-67075151d8e9


# ╔═╡ a282929a-1152-4155-94fc-f771910c1c44


# ╔═╡ edb0370f-1903-4cbd-9189-4c6b3f4e47f0


# ╔═╡ 0b9af736-fd5c-4a90-9e67-a9d4baaf7394


# ╔═╡ Cell order:
# ╠═09505e60-adc4-11ec-2592-89305c03f79b
# ╠═201f0440-adcc-11ec-2d76-537d71d4e8c3
# ╠═dce0b3d0-adcc-11ec-3831-8ba580c24628
# ╠═33f778c0-adcd-11ec-391a-a90fc64d7e57
# ╠═4a936f80-adcd-11ec-1f02-a3139bd6cc33
# ╠═3ee93cce-aded-11ec-0340-c964422c356e
# ╠═d8235a5e-adee-11ec-3d8a-f3d775f3d0d6
# ╠═d673fcb0-adee-11ec-1665-c3378544300c
# ╠═6bd36d90-adef-11ec-3757-351131787e2c
# ╠═6b902300-adef-11ec-077f-29269cccae9e
# ╠═6b787c50-adef-11ec-20aa-09b57ce48a95
# ╠═6b636db0-adef-11ec-1f9e-d1da52821ebb
# ╠═6b46e500-adef-11ec-0a0e-932d24773226
# ╠═6b2d90a0-adef-11ec-1907-cf705875db7c
# ╠═6b2a3540-adef-11ec-2aa3-ff4ee1d89da0
# ╠═6afeb872-adef-11ec-2aec-153be0251d6c
# ╠═6adf4990-adef-11ec-23ed-3f85728cc637
# ╠═6ac754c0-adef-11ec-3cc0-89149df14464
# ╠═6aa91e60-adef-11ec-0fa0-5f35d0be5e4b
# ╠═6a901820-adef-11ec-033c-b7e949b27457
# ╠═6a46da20-adef-11ec-377f-0deb7b256c2e
# ╠═d753ae50-adee-11ec-3d89-87e6b551a423
# ╠═68eaf3a0-adef-11ec-37bb-a560d39ac603
# ╠═d9e3d2ca-0ba1-42c9-b986-a5be115f622c
# ╠═fb34b59a-481d-46cf-b545-f4a9cf376dbb
# ╠═05b1a808-d1b4-4a12-a15b-67075151d8e9
# ╠═a282929a-1152-4155-94fc-f771910c1c44
# ╠═edb0370f-1903-4cbd-9189-4c6b3f4e47f0
# ╠═0b9af736-fd5c-4a90-9e67-a9d4baaf7394
