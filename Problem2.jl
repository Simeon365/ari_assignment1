### A Pluto.jl notebook ###
# v0.18.4

using Markdown
using InteractiveUtils

# ╔═╡ 7c3fbc10-b439-11ec-20f9-77066631b196
using Pkg

# ╔═╡ db3e5a62-845d-4d16-8b33-6972886114cc
Pkg.activate("Project.toml")

# ╔═╡ 479ab90e-f512-47bd-8158-f0eb849e2470
begin
using Flux, Statistics
using Flux: onehotbatch, onecold, crossentropy, throttle
using Base.Iterators: repeated, partition
using BSON
using ImageView
using Printf	
end

# ╔═╡ 6d170497-eaed-4106-8d7b-cad18e3a7937
function load_data(path)
	x=readdir(path,join=true)
	return x
end

# ╔═╡ 1b77aefa-ccdf-4320-b8f9-ec3c8bfb2387
begin
train_label = load_data("C:\\Users\\simeon\\Desktop\\chest_xray\\train")
test_label = load_data("C:\\Users\\simeon\\Desktop\\chest_xray\\test")
train_normal = load_data("C:\\Users\\simeon\\Desktop\\chest_xray\\train\\NORMAL")
train_pneumonia = load_data("C:\\Users\\simeon\\Desktop\\chest_xray\\train\\PNEUMONIA")
test_normal = load_data("C:\\Users\\simeon\\Desktop\\chest_xray\\test\\NORMAL")
test_pneumonia = load_data("C:\\Users\\simeon\\Desktop\\chest_xray\\test\\PNEUMONIA")
end

# ╔═╡ ea8ae6a9-38b3-490e-9484-31295c6d4dec
function AlexNet_Architecture(; imgsize=(227,227,3), nclasses=1000) 
    out_conv_size = (imgsize[1]÷4 - 3, imgsize[2]÷4 - 3, 256)
    
    return Chain( 
		    Conv((11,11), imgsize[end]=>96, relu), MaxPool((3,3)),
            Conv((5,5), 96 => 256, relu), MaxPool((3,3)),
            Conv((3,3), 256 => 384, relu),
            Conv((3,3), 384 => 384, relu),
            Conv((3,3), 384 => 256, relu), MaxPool((3,3)),
            flatten,
            Dense(prod(out_conv_size), 4096, relu),
            Dense(4096, 4096, relu),
            Dense(4096, 1000)
           )
end 

# ╔═╡ 9a9c19a1-16e7-4ab6-8c78-e8212177b2a1
function loss(x, y)
    x_aug = x .+ 0.1f0*gpu(randn(eltype(x), size(x)))

    y_hat = AlexNet_Architecture(x_aug)
    return crossentropy(y_hat, y)
end

# ╔═╡ b4e92e64-2ff2-4906-b9d7-fc1cd45e4063
accuracy(x, y) = mean(onecold(model(x)) .== onecold(y))

# ╔═╡ f73f54c2-c77c-44a2-a1dd-cd605f7117f7
opt = ADAM(0.001);

# ╔═╡ ec7d4e0e-6839-447c-bcc3-9639346fe9e4
best_acc = 0.0

# ╔═╡ c2de48f7-6f2b-40f8-b1ae-ae6ea18bc201
last_improvement = 0

# ╔═╡ 18fd5e3f-f8e6-4e43-a2d1-091461e11a3c
accuracy_target = 0.97

# ╔═╡ 9086b524-aa8c-463e-94f9-b65ccb48a93c
max_epochs = 100

# ╔═╡ 822904f9-9c73-4eba-8c2a-4bb60175a4ea
for epoch_idx in 1:100
    global best_acc, last_improvement
    Flux.train!(loss, params(AlexNet_Architecture), train_normal, opt)

    
    acc = accuracy(train_normal...)
    @info(@sprintf("[%d]: Train accuracy: %.4f", epoch_idx, acc))
    
    
    acc = accuracy(test_normal...)
    @info(@sprintf("[%d]: Test accuracy: %.4f", epoch_idx, acc))

    if acc >= accuracy_target
        @info(" -> Early-exiting: We reached our target accuracy of $(accuracy_target*100)%")
        break
    end

    if epoch_idx - last_improvement >= 10
        @warn(" -> We're calling this converged.")
        break
    end
end

# ╔═╡ efccff3f-5d8a-49c5-ac59-9eb053ff835c


# ╔═╡ f2e7995e-33a6-40da-a0e1-1c39e5d20e58


# ╔═╡ fa417e3c-9702-4423-aa15-ea9f5cd0b93d


# ╔═╡ fc4708cc-b3bd-4277-894c-0e53a4aacc9a


# ╔═╡ 0b080be0-8b59-4dff-99bc-321d7f5ba73b


# ╔═╡ 1c081b34-9a0a-49f1-945d-4b10a55626a6


# ╔═╡ Cell order:
# ╠═7c3fbc10-b439-11ec-20f9-77066631b196
# ╠═db3e5a62-845d-4d16-8b33-6972886114cc
# ╠═479ab90e-f512-47bd-8158-f0eb849e2470
# ╠═6d170497-eaed-4106-8d7b-cad18e3a7937
# ╠═1b77aefa-ccdf-4320-b8f9-ec3c8bfb2387
# ╠═ea8ae6a9-38b3-490e-9484-31295c6d4dec
# ╠═9a9c19a1-16e7-4ab6-8c78-e8212177b2a1
# ╠═b4e92e64-2ff2-4906-b9d7-fc1cd45e4063
# ╠═f73f54c2-c77c-44a2-a1dd-cd605f7117f7
# ╠═ec7d4e0e-6839-447c-bcc3-9639346fe9e4
# ╠═c2de48f7-6f2b-40f8-b1ae-ae6ea18bc201
# ╠═18fd5e3f-f8e6-4e43-a2d1-091461e11a3c
# ╠═9086b524-aa8c-463e-94f9-b65ccb48a93c
# ╠═822904f9-9c73-4eba-8c2a-4bb60175a4ea
# ╠═efccff3f-5d8a-49c5-ac59-9eb053ff835c
# ╠═f2e7995e-33a6-40da-a0e1-1c39e5d20e58
# ╠═fa417e3c-9702-4423-aa15-ea9f5cd0b93d
# ╠═fc4708cc-b3bd-4277-894c-0e53a4aacc9a
# ╠═0b080be0-8b59-4dff-99bc-321d7f5ba73b
# ╠═1c081b34-9a0a-49f1-945d-4b10a55626a6
